package uk.co.profish.fishcraft;

import org.bukkit.plugin.java.JavaPlugin;


/**
 * Entry point for the plugin
 * 
 * @author Fish
 */
public final class FcMain extends JavaPlugin
{
    private FcListener _eventListener;
    private FcPlayerManager _playerDatabase;

    /**
     * Startup / reload server
     */
    @Override
    public void onEnable()
    {        
        // Instantiate player data storage
        _playerDatabase = new FcPlayerManager( this );  
        
        // Register events
        _eventListener = new FcListener(this, _playerDatabase);
        this.getCommand("fish").setExecutor(new FcCommandManager(_playerDatabase));        
        
        getLogger().info("FISHCRAFT enabled");
    }

    
    /**
     * Shutdown server
     */
    @Override
    public void onDisable()
    {
        // Clean up database
        _playerDatabase.shutdown();
        getLogger().info("FISHCRAFT disabled");
    }
}
