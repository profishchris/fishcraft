package uk.co.profish.fishcraft;

import uk.co.profish.fishcraft.Jobs.FcJobTitle;

/**
 * Simple struct to represent a db entry
 * 
 * @author Fish
 */
public class FcPlayer
{
    public String Name;
    public FcJobTitle Job;
     
    public FcPlayer(String playerName, FcJobTitle playerJob)
    {
        Name = playerName;
        Job  = playerJob;
    }    
}
