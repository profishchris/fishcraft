/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.profish.fishcraft.Jobs;

import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Player Job interface
 * 
 * @author Fish
 */
public interface FcPlayerJob
{

    /**
     * Process the PlayerInteractEvent
     * 
     * @param interactEvent
     * @return true if interaction was successful 
     */
    boolean handleInteraction(PlayerInteractEvent interactEvent);
    
}
