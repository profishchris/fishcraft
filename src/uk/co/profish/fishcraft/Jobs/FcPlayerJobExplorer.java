package uk.co.profish.fishcraft.Jobs;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;




public class FcPlayerJobExplorer extends FcPlayerJobBase implements FcPlayerJob
{   
    private static final int BLINK_DISTANCE = 5;
    
    
    /**
     * Call base constructor
     * @param aPlayer 
     */
    public FcPlayerJobExplorer(Player aPlayer)
    {
        super(aPlayer);
    }

    
    @Override
    public boolean handleInteraction(PlayerInteractEvent interactEvent)
    {        
        Player pc = interactEvent.getPlayer();
        
        // Get the block at out location
        Block currentBlock = pc.getWorld().getBlockAt( pc.getLocation() ); 
        
        // Get the direction of the right click
        if(interactEvent.getClickedBlock() == null)
        {
            System.out.println("No block clicked");
            return false;
        }           
        pc.getLocation().getDirection();
        
        BlockFace facing = interactEvent.getClickedBlock().getFace( currentBlock );
        
         System.out.println( "EXPLORER: Interaction fired" );
         
        if( facing == null)
        {
            System.out.println("No clicked block");
            return false;
        }
        
        // Check we have a clear path in front of us
        for( int i = 0; i < BLINK_DISTANCE; i++ )
        {
            // Get the next block in direction player is facing
            currentBlock = currentBlock.getRelative( facing );
            
            // If the path is blocked we cannot blink
            if( currentBlock.getType() != Material.AIR )
            {
                return false;
            }
            
            System.out.println( currentBlock.getLocation() );
        }
        
        
        
        
        // Path is clear, teleport to the block BLINK_DISTANCE in front
        pc.teleport( currentBlock.getLocation() );
        return true;
    }

     
}
