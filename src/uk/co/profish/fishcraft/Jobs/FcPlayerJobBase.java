package uk.co.profish.fishcraft.Jobs;

import org.bukkit.entity.Player;

/**
 * Base class for player jobs
 * @author Fish
 */
abstract public class FcPlayerJobBase
{
    protected int XP_COST = 1;    
    protected Player _player;      
    
    /**
     * Set the player
     * @param aPlayer 
     */
    protected FcPlayerJobBase(Player aPlayer)
    {
        _player = aPlayer;
    }
        
    
    /**
     * Deduct XP Levels from the player
     * @param levelCost 
     */
    protected void deductLevels(int levelCost)
    {
        // Reduce current level
        _player.getPlayer().giveExpLevels(-levelCost);
    }
    
    
    /**
     * Ensure player has enough XP Levels
     * 
     * @param levelCost
     * @return true if player can pay the levelCost
     */
    protected boolean canPlayerPerformTask(int levelCost)
    {
        // Check player has enough xp
        if(_player.getPlayer().getLevel() < levelCost) return false;        
        
        // Player has passed tests
        return true;
    }
}
