package uk.co.profish.fishcraft.Jobs;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import static org.bukkit.Material.DIAMOND_SPADE;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;


public class FcPlayerJobGatherer extends FcPlayerJobBase implements FcPlayerJob
{

    /**
     * Hand off to base constructor
     * @param aPlayer 
     */
    public FcPlayerJobGatherer(Player aPlayer)
    {
        super(aPlayer);
    }
    
    
    @Override
    public boolean handleInteraction(PlayerInteractEvent interactEvent)
    {
        switch (interactEvent.getItem().getType())
        {
             case DIAMOND_SPADE:
                if( canPlayerPerformTask(XP_COST) && 
                    interactEvent.getAction() != Action.RIGHT_CLICK_AIR )
                {                    
                    surveyColumn(XP_COST, interactEvent);
                    return true;
                }
        }
        
        // checks failed
        return false;
    }
    
    

    /**
     * Output the material of all blocks
     * in the direction of the clickedBlock that was clicked
     * 
     * @param levelCost
     */
    private void surveyColumn(int levelCost, PlayerInteractEvent interactEvent)
    {
        final int SURVEY_DEPTH = 10;       
             
        
        // Set the 'forward' direction
        BlockFace direction = interactEvent.getBlockFace().getOppositeFace();
        // Get the first block
        Block clickedBlock = interactEvent.getClickedBlock();   
        
        
        // Add all the materials to a list
        Map<Material, Integer> blockList = new HashMap<>();
        for (int i = 0; i < SURVEY_DEPTH; i++)
        {
            // Either add a material to the list
            // or increment an exisiting one
            Material mat = clickedBlock.getType();            
            if (blockList.containsKey(mat))
            {
                int value = blockList.get(mat);
                blockList.put(mat, value + 1);
            }
            else
            {
                blockList.put(clickedBlock.getType(), 1);
            }

            // Get the next block in the given direction
            clickedBlock = clickedBlock.getRelative(direction);
        }

        // Build and send message
        StringBuilder message = new StringBuilder("\nSurvey Results\n");
        for (Map.Entry entry : blockList.entrySet())
        {
            // Format results
            message.append(String.format("%s: %s\n",
                    entry.getKey().toString().replace("_", " ").toLowerCase(Locale.UK),
                    entry.getValue().toString()));
        }
        _player.getPlayer().sendMessage(ChatColor.BLUE + message.toString());
        
        deductLevels(levelCost);
    }
}
