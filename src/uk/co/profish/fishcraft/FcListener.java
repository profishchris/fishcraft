/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.co.profish.fishcraft;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import static uk.co.profish.fishcraft.Jobs.FcJobTitle.EXPLORER;
import uk.co.profish.fishcraft.Jobs.FcPlayerJob;
import uk.co.profish.fishcraft.Jobs.FcPlayerJobExplorer;
import uk.co.profish.fishcraft.Jobs.FcPlayerJobGatherer;

/**
 * Listens for player events
 * @author Fish
 */
public class FcListener implements Listener
{
    final static int XP_COST = 1;
    
    // Refrences to main data
    private FcPlayerManager _playerManager = null;
    private FcMain _plugin;

    
    /**
     * Registers the events in this class
     * @param master 
     */
    public FcListener(FcMain master, FcPlayerManager dataStore)
    {
        // Get refrences
        _plugin = master;
        _playerManager = dataStore;
        
        // Register events
        _plugin.getServer().getPluginManager().registerEvents(this, master);
    }

    /**
     * Hook into player interaction
     * @param event 
     */
    @EventHandler
    public void onInteraction(PlayerInteractEvent event)
    {
        // Catch all right click actions
        if ( event.getAction() == Action.RIGHT_CLICK_BLOCK ||
             event.getAction() == Action.RIGHT_CLICK_AIR)
        {
            // Generic job type
            FcPlayerJob job = null;
            
            // Instantiate job
            switch (_playerManager.getPlayerJob( event.getPlayer().getName() ))
            {
                case GATHERER:
                    job = new FcPlayerJobGatherer( event.getPlayer() );
                    break;
                    
                case EXPLORER:
                    job = new FcPlayerJobExplorer( event.getPlayer() );
                    break;
                    
                // Job not implemented yet, exit
                default:
                    return;
            }
            
            // Call interaction event
            if( job != null ) job.handleInteraction( event );
        }
    }

    
    /**
     * Catch player logins
     * @param event 
     */
    @EventHandler
    public void onLogin(PlayerJoinEvent event)
    {
        // Get the player who logged in
        Player playerRef = event.getPlayer();
        
        // Saftly add player to the server
        _playerManager.loadPlayer( playerRef.getName() );
    }
}
