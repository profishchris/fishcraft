package uk.co.profish.fishcraft;

import java.sql.DatabaseMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import uk.co.profish.fishcraft.Jobs.FcJobTitle;

/**
 * Handles caching of player data and updates the persistent database
 * 
 * On construction loads all the online players into the cache
 * Handles creation and updating of player objects
 *
 */
public class FcPlayerManager
{
    // Constants
    private static final String _logPrefix = "PlayerManager: ";
    private static final long _dataSyncFrequency = 20 * 10; // 1 sec = 20 ticks
    
    // Database constants - change these after migration
    private static final String _password = "fishmaster9";
    private static final String _user = "root";
    private static final String _host = "jdbc:mysql://localhost:3306/fcplayers";
    private static final String _dbName = "players";
    
    // Data stores
    private Connection _persistentData = null;
    private ConcurrentHashMap<String, FcPlayer> _cacheData = null;

    /**
     * Load the SQLDriver and connect to the Database Populate data in RAM
     *
     */
    public FcPlayerManager(FcMain plugin)
    {
        // Get reference to the server
        Server pluginServer = plugin.getServer();
        
        // Connect to db
        openDatabaseConnection();

        // Init cache
        _cacheData = new ConcurrentHashMap<>(pluginServer.getOnlinePlayers().length, 8, 8);
        addPlayersToCache(pluginServer.getOnlinePlayers());

        // Setup periodic data sync
        BukkitTask dataSync = new FcTask_SyncData(this, pluginServer).runTaskTimer(plugin, _dataSyncFrequency, _dataSyncFrequency);
    }
    
    

    /**
     * Attempt to load the database drivers and open the connection
     *
     * @return true if successful
     */
    private boolean openDatabaseConnection()
    {
        try
        {
            // Load db Drivers
            Class.forName("com.mysql.jdbc.Driver");

            try
            {
                // Attempt to open db
                _persistentData = DriverManager.getConnection(_host, _user, _password);
                System.out.println(_logPrefix + "Connection established");
                
                // Check for database table
                DatabaseMetaData dbData = _persistentData.getMetaData();
                ResultSet tables = dbData.getTables(null, null, _dbName, null);
                if(tables.next())
                {
                    System.out.println(_logPrefix + "Database found: " + _dbName);
                }
                else
                {
                    System.err.println(_logPrefix + "DATABASE MISSING: " + _dbName);
                    return false;
                }
                
                // Load completed OK
                return true;
            }
            catch (SQLException e)
            {
                // Connection failure
                System.out.println(_logPrefix + e.getMessage());
                System.out.println(_logPrefix + e.getMessage());
                _persistentData = null;

                return false;
            }
        }
        catch (ClassNotFoundException e)
        {
            // Driver class not found
            System.out.println(_logPrefix + e.getMessage());
            return false;
        }
    }

    /**
     * Add the given players to the cache from the database Players not in the database will be generated and stored
     * both in the cache and the database
     *
     * @param playerList
     * @return number of players found in database
     */
    private void addPlayersToCache(Player[] playerList)
    {
        // Load each player into the cache
        for (Player selectedPlayer : playerList)
        {
            loadPlayer(selectedPlayer.getName());
        }
    }

    /**
     * Get a player record from the database
     *
     * @param playerName
     * @return corresponding player, else
     */
    private FcPlayer selectPlayer(String playerName)
    {
        // SELECT * FROM fcplayers.players WHERE name='playerName';
        try (PreparedStatement statement = _persistentData.prepareStatement("SELECT * FROM fcplayers.players WHERE name=?;"))
        {
            // Create statment and insert data
            statement.setString(1, playerName);
            // Get the result
            ResultSet record = statement.executeQuery();

            // If we are not before first, then no results where found
            if (record.isBeforeFirst())
            {
                // Return the player
                record.first();
                return new FcPlayer(playerName, Enum.valueOf(FcJobTitle.class, record.getString("class")));
            }
            else
            {
                return null;
            }
        }
        catch (SQLException e)
        {
            // Failure
            System.out.println(_logPrefix + "selectPlayer " + e.getMessage());
            return null;
        }
    }

    /**
     * Insert an FcPlayer into the database, or update existing
     *
     * @param playerRef
     * @return true if player added to store
     */
    private boolean insertOrUpdatePlayer(FcPlayer playerRef)
    {
        // Convert to record
        return insertOrUpdatePlayer(playerRef.Name, playerRef.Job);
    }

    /**
     * Insert a player record into the database or update existing
     *
     * @param playerRef
     * @param className
     * @return true if player added to store
     */
    private boolean insertOrUpdatePlayer(String playerName, FcJobTitle playerClass)
    {
        // Insert a new player into the table
        // or update their current record
        try (PreparedStatement statement = _persistentData.prepareStatement("INSERT INTO `fcplayers`.`players`(`name`,`class`)VALUES(?,?) ON duplicate KEY UPDATE class=VALUES(class);"))
        {
            // Set statement variables (?)
            statement.setString(1, playerName);
            statement.setString(2, playerClass.toString());
            // Execute statement
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            System.err.println(_logPrefix + "insertPlayer " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Write cache to database
     * NOTE: This should be the only method that modifies the database
     *  
     */
    private void updateDatabaseFromCache()
    {        
        // Loop through cache
        for (FcPlayer cachedPlayer : _cacheData.values())
        {
            insertOrUpdatePlayer(cachedPlayer);
        }
    }

    /**
     * Remove unused entries from the cache
     * 
     * @param pluginServer 
     */
    private void cleanCache(Server pluginServer)
    {
        // Iterate through each cache entry
        Entry<String, FcPlayer> cacheEntry = null;
        Iterator< Entry<String, FcPlayer>> iter = _cacheData.entrySet().iterator(); 
        
        while(iter.hasNext())
        {
            // Get the next entry
            cacheEntry = iter.next();
            // If player is not online, remove them from cache
            if (pluginServer.getPlayer(cacheEntry.getValue().Name) == null)
            {
                _cacheData.remove(cacheEntry.getKey());
            }
        }
    }
    
    
    

    /**
     * Fetch player from database, else create a new one and insert into cache
     *
     * @param playerName
     * @return non-null player object
     */
    public FcPlayer loadPlayer(String playerName)
    {
        // Assume the player exists and retrieve from database
        String loadMethod = " LOADED as ";
        FcPlayer playerRecord = selectPlayer(playerName);


        // If player not found create one
        if (playerRecord == null)
        {
            playerRecord = new FcPlayer(playerName, FcJobTitle.GATHERER);
            loadMethod = " REGISTERED as ";
        }

        // Load player into the cache
        _cacheData.putIfAbsent(playerName, playerRecord);

        // Log result
        System.out.println(_logPrefix + playerName + loadMethod + playerRecord.Job.toString());

        return playerRecord;
    }

    /**
     * Update database and clean cache
     */
    public void storeAllPlayers(Server pluginServer)
    {
        updateDatabaseFromCache();
        cleanCache( pluginServer );
    }
    
    /**
     * Write cache to db and close connection
     */
    public void shutdown()
    {
        updateDatabaseFromCache();
        try
        {
            _persistentData.close();
        }
        catch (SQLException ex)
        {
            // Close failed
            System.out.println(_logPrefix + "Database CLOSE failed");
        }
    }

    /**
     * Retrieve a player from the db
     *
     * @param playerName
     * @return player class, NONE if not found
     */
    public FcJobTitle getPlayerJob(String playerName)
    {
        // Find player
        FcPlayer playerRecord = _cacheData.get(playerName);
        if (playerRecord != null)
        {
            return playerRecord.Job;
        }

        return FcJobTitle.NONE;
    }

    /**
     * Set the player to the given job
     *
     * @param playerName
     * @param job
     * @return true if update succeeded
     */
    public boolean setPlayerJob(String playerName, FcJobTitle job)
    {
        System.out.println("CACHE PRE-UPDATE");
        System.out.println(cacheToString());
        FcPlayer cachedPlayer = _cacheData.get(playerName);
        if (cachedPlayer != null)
        {
            cachedPlayer.Job = job;

            System.out.println("CACHE POST-UPDATE");
            System.out.println(cacheToString());
            return true;
        }

        return false;
    }

    /**
     * Convert cache into human readable string
     *
     * @return
     */
    public String cacheToString()
    {
        StringBuilder stringObject = new StringBuilder();
        for (FcPlayer cachedPlayer : _cacheData.values())
        {
            stringObject.append(cachedPlayer.Name).append(" CACHED as ").append(cachedPlayer.Job.toString());
        }

        return stringObject.toString();
    }
}
