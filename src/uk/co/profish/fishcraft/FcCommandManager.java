package uk.co.profish.fishcraft;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import uk.co.profish.fishcraft.Jobs.FcJobTitle;

/**
 * Handles player text commands
 * 
 * @author Fish
 */
public class FcCommandManager implements CommandExecutor
{
    // Refrence to player data
    private FcPlayerManager _playerManager = null;
    
    
    /**
     * Maintain pointer to dataStore
     * @param datastore 
     */
    public FcCommandManager(FcPlayerManager dataStore)
    {
        _playerManager = dataStore;
    }
    

    /**
     * Parse text commands
     * 
     * @param sender
     * @param cmd
     * @param label
     * @param args
     * @return 
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {

        if (cmd.getName().equalsIgnoreCase("fish"))
        {
            Player thePlayer = (Player) sender;

            // The first argument is the command
            // Check command has been supplied
            if (args.length < 1)
            {
                return false;
            }
            
            
            String command = args[0].toLowerCase();
            switch (command)
            {
                case "light":
                    // Spawn torch at player's feet
                    Location playerLoc = thePlayer.getLocation();
                    Block targetBlock = thePlayer.getWorld().getBlockAt(playerLoc);

                    targetBlock.setType(Material.TORCH);
                    break;


                case "xp":
                    // Give player xp, possibly specified 
                    int levelMod;
                    try
                    {
                        levelMod = Integer.parseInt(args[1]);
                    }
                    catch (Exception e)
                    {
                        levelMod = 5;
                    }

                    thePlayer.giveExpLevels(levelMod);
                    break;


                case "equip":
                    // Clear inventory
                    thePlayer.getInventory().clear();
                    // Add Items
                    ItemStack iss;
                    iss = new ItemStack(Material.DIAMOND_AXE);
                    thePlayer.getInventory().addItem(iss);
                    iss = new ItemStack(Material.TORCH, 64);
                    thePlayer.getInventory().addItem(iss);
                    iss = new ItemStack(Material.DIAMOND_PICKAXE);
                    thePlayer.getInventory().addItem(iss);
                    iss = new ItemStack(Material.SHEARS);
                    thePlayer.getInventory().addItem(iss);
                    iss = new ItemStack(Material.DIAMOND_SPADE);
                    thePlayer.getInventory().addItem(iss);

                    // Wellbeing
                    thePlayer.setHealth(thePlayer.getMaxHealth());
                    thePlayer.setFoodLevel(20);
                    thePlayer.setLevel(2013);
                    thePlayer.getWorld().setDifficulty(Difficulty.PEACEFUL);
                    // Time of day
                    thePlayer.getWorld().setTime(1 * 1000);
                    // Weather
                    thePlayer.getWorld().setWeatherDuration(1000);
                    thePlayer.getWorld().setStorm(false);
                    thePlayer.getWorld().setThundering(false);
                    break;


                case "home":
                    // Either bed or default spawn point
                    Location home = thePlayer.getBedSpawnLocation();
                    if (home == null)
                    {
                        home = thePlayer.getWorld().getSpawnLocation();
                    }

                    thePlayer.teleport(home);
                    break;


                case "fly":
                    // Toggle fly
                    boolean canFly = !thePlayer.getAllowFlight();
                    thePlayer.setAllowFlight(canFly);
                    thePlayer.sendMessage("Can fly = " + canFly);
                    break;
                    
                case "class":
                    // Change the players class
                    if(args.length > 1)
                    {
                        // Convert string into enum
                        String classArg = args[1].trim().toUpperCase();
                        try
                        {
                            FcJobTitle title = Enum.valueOf( FcJobTitle.class, classArg );
                                    
                            switch (title)
                            {
                                case GATHERER:
                                    if (_playerManager.setPlayerJob(thePlayer.getName(), FcJobTitle.GATHERER))
                                        thePlayer.sendMessage("Class set: Gatherer");
                                    break;

                                case EXPLORER:
                                    if (_playerManager.setPlayerJob(thePlayer.getName(), FcJobTitle.EXPLORER))
                                        thePlayer.sendMessage("Class set: Explorer");
                                    break;

                                default:
                                    thePlayer.sendMessage("No Such class");
                                    break;
                            }
                        }
                        catch(Exception e)
                        {
                            // Enum parse failed
                            thePlayer.sendMessage("No Such class");
                        }
                        
                    }
                    else
                    {
                        thePlayer.sendMessage( "Class: " + _playerManager.getPlayerJob(thePlayer.getName()) );
                    }
                    break;

                    
                default:
                    // Player enterd an invalid command
                    thePlayer.sendMessage("Fish does not understand " + command);
                    break;
            }


        }

        return true;
    }
}
