package uk.co.profish.fishcraft;

import org.bukkit.Server;
import org.bukkit.scheduler.BukkitRunnable;
 
/**
 * Updates the database from the cache periodically
 * 
 * @author Fish
 */
public class FcTask_SyncData extends BukkitRunnable {
 
    private final FcPlayerManager _playerManager;
    private final Server _pluginServer;
 
    
    public FcTask_SyncData(FcPlayerManager manager, Server server) 
    {
        _playerManager = manager;
        _pluginServer = server;
    } 
    
    
    @Override
    public void run() 
    {
        _playerManager.storeAllPlayers(_pluginServer);
    }
 
}
